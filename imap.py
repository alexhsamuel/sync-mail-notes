import email.header
import email.parser
from   imapclient import IMAPClient
import json
import logging
import lxml.etree
import quopri

def decode(data):
    string, encoding = email.header.decode_header(data)[0]
    if encoding is not None:
        string = string.decode(encoding)
    assert isinstance(string, str)
    return string


def indent(text, indent):
    return "".join( indent + l for l in text.splitlines() )


def html_to_markdown(doc):
    res = []
    start = True

    def _convert(el):
        nonlocal start

        match el.tag:
            case "div" | "p" | "ul" | "ol":
                if not start:
                    res.append("\n")
                    start = True
            case "br":
                res.append("\n")
                start = True
            case "body" | "span":
                pass
            case "li":
                res.append("- ")
            case "a":
                # FIXME:
                pass
            case "b":
                res.append("**")
            case _:
                assert False, f"unexpected tag: {el.tag}"

        if el.text:
            res.append(el.text)
            start = False

        for child in el:
            _convert(child)

        if el.tail:
            res.append(el.tail)
            start = False

        match el.tag:
            case "div" | "p" | "ul" | "ol":
                if not start:
                    res.append("\n")
                    start = True
            case "br":
                pass
            case "body" | "span":
                pass
            case "li":
                pass
            case "a":
                # FIXME:
                pass
            case "b":
                res.append("**")
            case _:
                assert False, f"unexpected tag: {el.tag}"

    assert doc.tag == "html"
    for el in doc:
        match el.tag:
            case "head":
                pass
            case "body":
                _convert(el)

    return "".join(res)


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

with open("/home/alex/sec/imap-notes.json") as file:
    sec = json.load(file)

cl = IMAPClient(sec["hostname"], use_uid=True)
cl.login(sec["username"], sec["password"])
cl.select_folder("Notes")

rfc822_parser = email.parser.BytesParser()
html_parser = lxml.etree.HTMLParser()

msg_ids = cl.search()
for msg_id, data in cl.fetch(msg_ids, ["RFC822"]).items():
    msg = rfc822_parser.parsebytes(data[b"RFC822"])

    uti = msg.get("X-Uniform-Type-Identifier", None)
    if uti != "com.apple.mail-note":
        logger.warning(f"wrong Uniform-Type-Identifier: {uti}")
        continue

    try:
        uui = msg["X-Universally-Unique-Identifier"]
    except KeyError:
        logger.warning(f"missing Universally-Unique-Identifier")
        continue

    subject = decode(msg["Subject"])
    content_type = msg.get_content_type()
    if content_type != "text/html":
        logger.warning(f"wrong Content-Type: {content_type}")
        continue

    html = quopri.decodestring(msg.get_payload()).decode()

    basename = subject.replace("/", "_")
    with open(f"notes/{basename}.html", "w") as file:
        file.write(html)

    doc = lxml.etree.fromstring(html, html_parser)
    markdown = html_to_markdown(doc)

    with open(f"notes/{basename}.md", "w") as file:
        file.write(markdown)

