For this to be usable, we need:

One way:

- complete conversion of HTML → Markdown

Two way:

- reverse conversion of Markdown → HTML
- confirm that roundtrip works
- detect changes to local files
- store the unique ID somewhere in or next to (DB?) the local files
- upload local files
- figure out what happens when the note name / subject changes

Bonus:

- backup to tarfile
- backup to git
- poll loop to sync
- inotify for local file changes
- is there an IMAP push mechanism we can use?

