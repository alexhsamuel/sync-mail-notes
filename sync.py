"""
Syncs notes stored in a mail folder, via JMAP.
"""

from   jmapc import Client, Ref, MailboxQueryFilterCondition, EmailQueryFilterCondition, TypeState, Email
from   jmapc.methods import MailboxQuery, MailboxGet, MailboxGetResponse
from   jmapc.methods import EmailGet, EmailGetResponse, EmailChanges, EmailQuery, EmailQueryResponse

HOST = "api.fastmail.com"
API_TOKEN = open("/home/alex/sec/jmap-sync-api-token").read()

client = Client.create_with_api_token(host=HOST, api_token=API_TOKEN)

# MailNotes id=f7432d2a-64ab-407c-b976-48df0508c17b

if True:
    rsp = client.request([MailboxQuery(), MailboxGet(ids=Ref("/ids"))])
    rsp = rsp[1].response
    for mailbox in rsp.data:
        print(f"{mailbox.id} {mailbox.role!s:12s} {mailbox.name}")


if False:
    # Prepare two methods to be submitted in one request
    # The first method, Mailbox/query, will locate the ID of the Inbox folder
    # The second method, Mailbox/get, uses a result reference to the preceding
    # Mailbox/query method to retrieve the Inbox mailbox details
    methods = [
        MailboxQuery(filter=MailboxQueryFilterCondition(name="MailNotes")),
        MailboxGet(ids=Ref("/ids")),
    ]

    # Call JMAP API with the prepared request
    results = client.request(methods)

    # Retrieve the InvocationResponse for the second method. The InvocationResponse
    # contains the client-provided method ID, and the result data model.
    method_2_result = results[1]

    # Retrieve the result data model from the InvocationResponse instance
    method_2_result_data = method_2_result.response

    # Retrieve the Mailbox data from the result data model
    assert isinstance(method_2_result_data, MailboxGetResponse), "Error in Mailbox/get method"
    print(method_2_result_data)
    mailboxes = method_2_result_data.data

    # Although multiple mailboxes may be present in the results, we only expect a
    # single match for our query. Retrieve the first Mailbox from the list.
    mailbox = mailboxes[0]

    # Print some information about the mailbox
    print(mailbox)
    print()

    rsp = client.request([EmailQuery(filter=EmailQueryFilterCondition(in_mailbox=mailbox.id))])
    rsp = rsp[0].response
    assert isinstance(rsp, EmailQueryResponse)
    rsp = client.request([EmailGet(ids=rsp.ids)])
    rsp = rsp[0].response
    assert isinstance(rsp, EmailGetResponse)
    print(f"state: {rsp.state}")
    print()
    for email in rsp.data:
        assert isinstance(email, Email)
        print(email.id, email.blob_id, email.thread_id, format(email.size, "8d"), email.sent_at, email.received_at)



if False:
    # I don't know why, we don't seem to get an event when a note is updated.
    state = {}

    for event in client.events:
        for account_id, new_state in event.data.changed.items():
            try:
                prev_state = state[account_id]
            except KeyError:
                prev_state = TypeState()

            if new_state != prev_state and new_state.email != prev_state.email:
                print(f"email changed: {prev_state.email} → {new_state.email}")
                if prev_state is not None:
                    results = client.request([
                        EmailChanges(since_state=prev_state),
                    ])
                    print(results)
                    print()

            state[account_id] = new_state


